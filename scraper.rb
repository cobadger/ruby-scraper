require "nokogiri"
require "httparty"
require "byebug"
require "csv"

def get_states
  url = "https://www.raceentry.com/race-chip-timing-systems"
  unparsed_page = HTTParty.get(url)
  parsed_page = Nokogiri::HTML(unparsed_page)

  states = Array.new
  state_sections = parsed_page.css('div.timer-container div.row div.col-md-6')
  state_sections.each do |state_section|
    state = {
      name: state_section.css('a')[0].text,
      url: state_section.css('a')[0].attributes["href"].value,
    }
    states << state
  end
end

def scan_state(state)
  scanned_state = {
    name: state.css('input')[0].attributes["value"].value,
    url: state.css('a')[0].attributes["href"].value,
  }
end

def scan_all_states
  states_array = Array.new
  states = get_states
  states.each do |state_obj|
    scanned_state = scan_state(state_obj)
    states_array << scanned_state
  end
  return states_array
end

# Initiate timers array.
timers = Array.new

scanned_states = scan_all_states
scanned_states.each do |scanned_state|
  # Handy debugging line - uncomment next line to just get a few timers.
  # break unless scanned_state[:name] == "Alabama"

  # Make get request of the state url.
  unparsed_state_page = HTTParty.get(scanned_state[:url])
  parsed_state_page = Nokogiri::HTML(unparsed_state_page)
  state_timers = Array.new
  timer_listings = parsed_state_page.css('div.custom-container div.row div.row a')
  timer_listings.each do |timer_listing|
    timer = {
      name: timer_listing.children[0].text,
      url: timer_listing.attributes["href"].value,
    }
    puts "Adding #{timer[:name]}"
    puts ""
    timers << timer
  end
end

CSV.open('timers.csv','wb', col_sep: "\t") do |csvfile|
  csvfile << ["company", "contact", "phone", "email", "city", "state", "url"]
  timers.each do |row|
    unparsed_timer_page = HTTParty.get(row[:url])
    parsed_timer_page = Nokogiri::HTML(unparsed_timer_page)
    email_string = parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
            [2].css('div.col-sm-8.word-wrap').children.text
    if email_string.include? "&"
      emails = email_string.split("&")
      emails.each do |email|
        email_address = email.strip!
        timer = {
          company: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                  [4].css('div.col-sm-8.word-wrap').children.text,
          contact: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                  [0].css('div.col-sm-8.word-wrap').children.text,
          phone: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                  [1].css('div.col-sm-8.word-wrap').children.text,
          email: email_address,
          city: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                  [3].css('div.col-sm-8.word-wrap').children.text.split(",")[0],
          state: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                  [3].css('div.col-sm-8.word-wrap').children.text.split(",")[1]\
                  .split(" ")[0],
          url: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                  [4].css('div.col-sm-8.word-wrap').children[0].attributes["href"]\
                  .value,
        }
        puts "Saving #{timer[:company]}"
        puts ""
        csvfile << timer.values
      end
    else
      timer = {
        company: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [4].css('div.col-sm-8.word-wrap').children.text,
        contact: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [0].css('div.col-sm-8.word-wrap').children.text,
        phone: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [1].css('div.col-sm-8.word-wrap').children.text,
        email: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [2].css('div.col-sm-8.word-wrap').children.text,
        city: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [3].css('div.col-sm-8.word-wrap').children.text.split(",")[0],
        state: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [3].css('div.col-sm-8.word-wrap').children.text.split(",")[1]\
                .split(" ")[0],
        url: parsed_timer_page.css('div.side_tab_panel div.white-box div.row')\
                [4].css('div.col-sm-8.word-wrap').children[0].attributes["href"]\
                .value,
      }
      puts "Saving #{timer[:company]}"
      puts ""
      csvfile << timer.values
    end
  end
end
